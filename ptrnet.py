"""
Refs:
- https://arxiv.org/pdf/1506.03134.pdf (Vinyals et al)
- https://arxiv.org/pdf/1409.0473.pdf (Bahdanau et al)
- https://bastings.github.io/annotated_encoder_decoder/
"""

import torch
import torch.nn as nn
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence

from logger import logger


class Encoder(nn.Module):
    """
    Computes a vector representation of an input sequence
    """

    def __init__(self, input_size, hidden_size):
        super().__init__()
        self.rnn = nn.GRU(
            input_size, hidden_size, num_layers=1, batch_first=True, bidirectional=True
        )

    def forward(self, X, lengths, H=None):
        """
        Applies a bidirectional GRU to sequence of embeddings X.
        The input mini-batch X needs to be sorted by length.
        X should have dimensions [batch, time steps, features].

        Output contains the forwards and backwards representation at each time step
        H contains the hidden representation for the whole sequence
        """
        logger.debug(f"X.shape = {X.shape}")
        if H is not None:
            logger.debug(f"H.shape = {H.shape}")
        # Pack sequences to avoid evaluating RNN on padding time steps
        packed = pack_padded_sequence(X, lengths, batch_first=True)
        output, H = self.rnn(packed, H)
        # Repad the packed sequences to tensors
        output, _ = pad_packed_sequence(output, batch_first=True)
        # Concatenate forward and backward representations, then readd 1st dimension
        # so that we have (nlayers, batch size, 2 * hidden size)
        H = H.permute(1, 0, 2).flatten(1, 2)[None, :, :]
        logger.debug(f"output.shape = {output.shape}, H.shape = {H.shape}")
        return output, H


class Attention(nn.Module):
    def __init__(self, encoder_size, decoder_size, hidden_size):
        super().__init__()
        # Project encoder and decoder representations
        self.W1 = nn.Parameter(
            nn.init.xavier_normal_(torch.empty(encoder_size, hidden_size))
        )
        self.W2 = nn.Parameter(
            nn.init.xavier_normal_(torch.empty(decoder_size, hidden_size))
        )
        # Project to scalar value
        self.v = nn.Parameter(nn.init.xavier_normal_(torch.empty(hidden_size, 1)))
        self._proj_E = None

    def init_proj_E(self, E):
        logger.debug(f"E.shape = {E.shape}, W1.shape = {self.W1.shape}")
        self._proj_E = E.matmul(self.W1)

    def forward(self, d, padding_mask, E=None):
        """
        An implementation of Badhanau's attention mechanism
            u_j^i = v^t tanh(W_1 e_j + W_2 d_i) forall j in (1, ..., n)

            - Produces an attention score for each encoder output j with decoder output i
            - W_1 e_j, W_2 d_i and v must be in the same dimension
            - u_j^i is a scalar

        E = (batch_size, time_step, encoder_size)
        d = (n_layers, batch_size, decoder_size)

        Must apply a mask where there are padding values before softmaxing
        """
        if E:
            self.init_proj_E(E)
        # We extract the representations from the LAST layer of the decoder only
        # and reshape it so that it can be broadcasted to all timesteps in E
        d = d[-1][:, None, :]
        logger.debug(f"d.shape = {d.shape}, W2.shape = {self.W2.shape}")
        proj_D = d.matmul(self.W2)
        logger.debug(
            f"proj_E.shape = {self._proj_E.shape}, proj_D.shape = {proj_D.shape}, v.shape = {self.v.shape}"
        )
        # (batch_size, time_step, 1)
        U = torch.tanh(self._proj_E + proj_D).matmul(self.v)
        logger.debug(f"U.shape = {U.shape}")
        U[padding_mask] = float("-inf")
        # Take the softmax on the timesteps dimension
        return torch.softmax(U, 1)


class Decoder(nn.Module):
    """
    Decodes an input element by using the encoded representation of the sequence
    """

    def __init__(self, input_size, hidden_size):
        super().__init__()
        # Use GRUCell because we feed the inputs ourselves
        self.rnn = nn.GRU(
            input_size, hidden_size, 1, batch_first=True, bidirectional=False
        )

    def forward(self, z, h_d):
        """
        A single step of the decoder RNN
        """
        logger.debug(f"z.shape = {z.shape}, h_d.shape = {h_d.shape}")
        return self.rnn(z, h_d)


class PtrNet(nn.Module):
    """
    Simple PtrNet: seq2seq model that outputs an attention distribution over its input.
    Essentially doing soft selection.
    """

    # TODO: check multilayer + varying dimensions
    # TODO: check span extraction use case
    def __init__(self, vocab_size, embed_size=300, hidden_size=512, proj_size=128):
        super().__init__()
        logger.debug(
            f"vocab_size = {vocab_size}, embed_size = {embed_size}, "
            f"hidden_size = {hidden_size}, proj_size = {proj_size}"
        )
        self.vocab_size = vocab_size
        self.embed_size = embed_size
        self.hidden_size = hidden_size
        self.proj_size = proj_size

        self.embed = nn.Embedding(self.vocab_size, self.embed_size)

        self.encoder = Encoder(embed_size, hidden_size)
        # Encoder is bidirectional
        self.decoder = Decoder(embed_size + 2 * hidden_size, hidden_size)
        self.attention = Attention(2 * hidden_size, hidden_size, proj_size)
        self.proj_to_decoder = nn.Linear(2 * hidden_size, hidden_size, bias=False)

    def forward(self, X_pad, X_lens):
        """
        According to the original paper, we do the following:
            During encoding:
                - Get hidden state for <EOS> token and use that as init hidden
                - Pass X_pad
            During decoding:
                - Init hidden with encoder hidden state output
                - Init sequence with <BOS> token
                - Last decoder target is the <EOS> token
        """
        X_emb = self.embed(X_pad)
        logger.debug(f"X_emb.shape = {X_emb.shape}")
        E, d = self.encoder(X_emb, X_lens)

        # Since encoder is bidirectional, it doubles the number of features
        # by concatenation. Reproject back to hidden_size
        d = self.proj_to_decoder(d)
        logger.debug(f"d.shape = {d.shape}")
        # Compute E * W1 outside of the loop for efficiency
        self.attention.init_proj_E(E)

        atts = []
        X_padding_mask = X_pad == 0
        # Unroll the decoder one step at a time
        for step, y in enumerate(X_emb.split(1, dim=1)):
            logger.debug(f"step = {step}")
            # Get the attention coefficients, this is the PtrNet output
            att = self.attention(d, X_padding_mask)
            atts.append(att)
            # Weight the encoder output by the attention and sum over the timesteps
            logger.debug(f"E.shape = {E.shape}, att.shape = {att.shape}")
            c = (E * att).sum(1)
            # Remove timestep dimension
            y = y.squeeze(1)
            logger.debug(f"y.shape = {y.shape}, c.shape = {c.shape}")
            # Concatenate on the timesteps and add one dimension for timesteps
            z = torch.cat([y, c], axis=1)[:, None, :]
            # D becomes next word during inference, during training use next Y_emb
            logger.debug(f"z.shape = {z.shape}, d.shape = {d.shape}")
            _, d = self.decoder(z, d)
            logger.debug(f"d.shape = {d.shape}")
        # We processed each timestep individually
        # and each has an attention distribution over the input sequence
        # (batch_size, time_step, seq_len)
        return torch.cat(atts, dim=2).permute(0, 2, 1)
