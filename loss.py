from torch.nn import CrossEntropyLoss

from logger import logger


class SimpleLoss:
    loss_func = CrossEntropyLoss(ignore_index=-1)

    def __call__(self, Y_pred, Y_pad):
        logger.debug(f"Y_pred.shape = {Y_pred.shape}, Y_pad.shape = {Y_pad.shape}")
        Y_pred, Y_pad = Y_pred.flatten(0, 1), Y_pad.flatten()
        logger.debug(f"Y_pred.shape = {Y_pred.shape}, Y_pad.shape = {Y_pad.shape}")
        return self.loss_func(Y_pred, Y_pad)
