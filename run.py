import random
from itertools import islice
from pathlib import Path

import click
import numpy as np
import torch
from torch.nn.utils.rnn import pad_sequence
from torch.optim import AdamW
from torch.optim.lr_scheduler import ReduceLROnPlateau

from logger import logger
from ptrnet import PtrNet
from loss import SimpleLoss


# TODO: replace by enum or dict class
TOKENS = {"<PAD>": 0}
N_SPECIAL_TOKENS = len(TOKENS)


def generate_sequences(n_seqs=32, max_int=50, max_seq_len=10):
    """
    Generate (n_seqs * n_batches) data pairs to learn a model that sorts sequences
    Generate sequences in ]N_SPECIAL_TOKENS, N_SPECIAL_TOKENS + max_seq_len]
    """
    while True:
        X, Y = [], []
        for _ in range(n_seqs):
            seq_len = random.randint(1, max_seq_len)
            # Generate a sequence of seq_len random unique elements unsorted elements
            seq = (N_SPECIAL_TOKENS + torch.randperm(max_int))[:seq_len]
            X.append(seq)
            # Get the indices corresponding to the sorted sequence
            # Append <EOS> token
            Y.append(seq.argsort().type(torch.LongTensor))

        X_pad = pad_sequence(X, batch_first=True, padding_value=0)
        logger.debug(f"X_pad.shape = {X_pad.shape}")
        Y_pad = pad_sequence(Y, batch_first=True, padding_value=-1)
        logger.debug(f"Y_pad.shape = {Y_pad.shape}")
        # Compute input sequence lengths for pack_padded_sequence in encoder RNN
        X_lens = np.array([x.shape[0] for x in X])
        # Get index sorted in decreasing sequence length -- required for pack_padded_sequence
        sorted_idx = X_lens.argsort()[
            ::-1
        ].copy()  # for some weird reason need copy here???
        yield X_pad[sorted_idx, :], X_lens[sorted_idx].tolist(), Y_pad[sorted_idx, :]


@click.command()
@click.argument("output_folder", type=click.Path())
def main(output_folder):
    max_int = 20
    max_seq_len = min(7, max_int)
    val_max_seq_len = min(max_seq_len + 3, max_int)
    batch_size = 8192

    train_sequences = generate_sequences(batch_size, max_int, max_seq_len)
    val_sequences = generate_sequences(batch_size, max_int, val_max_seq_len)

    vocab_size = max_int + N_SPECIAL_TOKENS
    embed_size = 32
    model = PtrNet(vocab_size, embed_size, 128, 64)
    optimizer = AdamW(model.parameters(), lr=0.01)
    scheduler = ReduceLROnPlateau(optimizer, factor=0.5)
    loss_func = SimpleLoss()
    train(
        output_folder,
        model,
        optimizer,
        scheduler,
        loss_func,
        train_sequences,
        5,
        val_sequences,
        1,
        n_epochs=500,
        min_lr=1e-5
    )


def train(
    out_folder,
    model,
    optimizer,
    scheduler,
    loss_func,
    train_sequences,
    n_batches_train,
    val_sequences,
    n_batches_val,
    n_epochs=10,
    min_lr=1e-5
):
    if isinstance(out_folder, str):
        out_folder = Path(out_folder)
    out_folder = out_folder.resolve()
    out_folder.mkdir(exist_ok=True, parents=True)

    logger.info(
        f"Epoch\t|\tTrain loss\t|\tVal loss\t|\t Learning rate\t|\tVal loss [best]\n"
    )
    best_val_loss = float("inf")
    for epoch in range(n_epochs):
        model.train()
        train_loss = train_ind = 0
        for X_pad, X_lens, Y_pad in islice(train_sequences, n_batches_train):
            # Model must predict given same input sequence
            # and output softmax values on the input sequence
            Y_pred = model(X_pad, X_lens)
            loss = loss_func(Y_pred, Y_pad)
            train_loss += loss.item()
            train_ind += 1
            loss.backward()  # compute gradients
            optimizer.step()  # take optimizer step
            optimizer.zero_grad()  # reset gradients
        train_loss = train_loss / train_ind
        model.eval()
        val_loss = 0
        for X_val_pad, X_val_lens, Y_val_pad in islice(val_sequences, n_batches_val):
            Y_val_pred = model(X_val_pad, X_val_lens)
            loss = loss_func(Y_val_pred, Y_val_pad)
            val_loss += loss.item()
        rand_ind = random.randint(0, Y_val_pad.shape[0])
        logger.info(
            f"\n{X_val_pad[rand_ind].numpy()}\t->\t{X_val_pad[rand_ind][Y_val_pad[rand_ind]].numpy()}\n"
            f"\n{Y_val_pred.argmax(2)[rand_ind].numpy()}\n{Y_val_pad[rand_ind].numpy()}\n"
        )
        lr = optimizer.param_groups[0]["lr"]
        logger.info(
            f"{epoch}\t|\t{train_loss:.4f}\t|\t{val_loss:.4f}\t|\t{lr:.4f}\t|"
            f"\t{best_val_loss:.4f}\n"
        )
        if val_loss < best_val_loss:
            best_val_loss = val_loss
            torch.save(model.state_dict(), out_folder / "best-model.pt")
        scheduler.step(val_loss, epoch)
        if lr < min_lr:
            logger.warning(f"Exiting training early because lr < min_lr: {lr} < {min_lr}")
            break


if __name__ == "__main__":
    main()
